import todosList from "./todos.json";

import {
    TOGGLE_TODO,
    ADD_TODO,
    DELETE_TODO,
    CLEAR_COMPLETED_TODOS
} from "./action"

const initialState = {
    todos: todosList,
    input: ""
}

function reducer(state = initialState, action){
    switch (action.type){
        case TOGGLE_TODO:
        const updatedTodos = state.todos.filter(item => {
            if(item.id === action.todo.id){
                item.completed = !action.todo.completed
            }
            return item
        });
        return ({ ...state, todos: updatedTodos })
            case ADD_TODO:
            
            let tempState = state.todos.slice()
            tempState.push(
                {
                    userId: 1,
                    id: Math.ceil(Math.random() * 100000),
                    title: action.input,
                    completed: false
                }
            )
            return ({ ...state, todos: tempState})
        case DELETE_TODO:
            const newTodos = state.todos.filter(item => item.id !== action.id);
            return ({ ...state, todos: newTodos });
        case CLEAR_COMPLETED_TODOS:
            let tempTodos = state.todos.filter(
                todo => todo.completed === false
            )
            return ({ ...state, todos: tempTodos })
        default:
            return state
    }
}

export default reducer